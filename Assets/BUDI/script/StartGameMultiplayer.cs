using Gameson;
using KartToon;
using KartToon.UI;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PowerslideKartPhysics
{
    public class StartGameMultiplayer : Page
    {
        [SerializeField] Transform[] startPost;
        [SerializeField] GameObject[] botKart;

        [SerializeField] GameObject kartTemp;
        private void Start()
        {
            PhotonNetwork.Instantiate("KartMultiplayer/"+GameManager.Instance.GetCharacter().name, startPost[PhotonNetwork.LocalPlayer.ActorNumber].position, Quaternion.identity);
        }
    }
}
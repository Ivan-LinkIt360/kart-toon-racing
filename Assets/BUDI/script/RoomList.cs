using Photon.Pun;
using KartToon.UI;
using Photon.Realtime;
using Gameson;

namespace KartToon.Multiplayer
{
    public class RoomList : MonoBehaviourPunCallbacks
    {
        public override void OnJoinedRoom()
        {
            GameManager.Instance.ChangeState(GameState.Multiplayer);
            PhotonNetwork.LoadLevel(2);
        }
        public void JoinRoom()
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 8;
            PhotonNetwork.JoinOrCreateRoom("a",roomOptions,null);
        }
    }
}
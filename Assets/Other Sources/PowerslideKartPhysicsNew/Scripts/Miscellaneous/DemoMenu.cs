﻿// Copyright (c) 2021 Justin Couch / JustInvoke
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PowerslideKartPhysics
{
    // This class controls the menu in the demo and spawns karts
    public class DemoMenu : MonoBehaviour
    {
        public Vector3 spawnPoint = Vector3.zero;
        public Vector3 spawnDir = Vector3.forward;
        public GameObject uiContainer;
        public KartCamera kartCam;

        public List <GameObject> CP;
        public List <CarCPManager> Player;

        public List <Slider> parameterPositionPlayer;

        public GameObject kart;
        
        public GameObject[] characterPrefabs;
	    //public Transform spawnPoint;

        public bool MultiplayerScene;

        private void Awake()
        {
            // Hide UI when showing the menu
            if (uiContainer != null)
            {
                //uiContainer.SetActive(false);
            }
        }

        /*public void SpawnKart()
        {
            // Spawn a given kart at the spawn point
            Kart newKart = null;
            if (kart != null)
            {
                //newKart = Instantiate(kart, spawnPoint, Quaternion.LookRotation(spawnDir.normalized, Vector3.up)).GetComponent<Kart>();
            }

            // Show the UI and connect it to the spawned kart
            if (uiContainer != null)
            {
                UIControl uiController = uiContainer.GetComponent<UIControl>();
                if (uiController != null)
                {
                    uiController.Initialize(newKart);
                }

                uiContainer.SetActive(true);
            }

            // Connect the camera to the spawned kart
            if (kartCam != null)
            {
                kartCam.Initialize(newKart);
            }

            gameObject.SetActive(false);
        }*/

        /*public void SpawnKart()
        {
            // Spawn a given kart at the spawn point
            Kart newKart = null;
            if (kart != null)
            {
                //int selectedCharacter = PlayerPrefs.GetInt("selectedCharacter");
                //GameObject prefab = characterPrefabs[selectedCharacter];
                //GameObject clone = Instantiate(prefab, spawnPoint, Quaternion.LookRotation(spawnDir.normalized, Vector3.up)).GetComponent<Kart>();
                //newKart = Instantiate(prefab, spawnPoint, Quaternion.LookRotation(spawnDir.normalized, Vector3.up)).GetComponent<Kart>();
                //newKart = Instantiate(prefab, spawnPoint, Quaternion.LookRotation(spawnDir.normalized, Vector3.up)).GetComponent<Kart>();
            }

            // Show the UI and connect it to the spawned kart
            if (uiContainer != null)
            {
                UIControl uiController = uiContainer.GetComponent<UIControl>();
                if (uiController != null)
                {
                    uiController.Initialize(newKart);
                }

                uiContainer.SetActive(true);
            }

            // Connect the camera to the spawned kart
            if (kartCam != null)
            {
                kartCam.Initialize(newKart);
            }

            gameObject.SetActive(false);
        }*/

        // Visualize the kart spawn point
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(spawnPoint, 0.5f);
            Gizmos.DrawRay(spawnPoint + spawnDir.normalized * 0.5f, spawnDir.normalized);
        }

        void Start(){
            if (MultiplayerScene == false)
            {
                Kart newKart = null;
                if (kart != null)
                {
                    int selectedCharacter = PlayerPrefs.GetInt("selectedCharacter");
                    GameObject kart = characterPrefabs[selectedCharacter];
                    //GameObject clone = Instantiate(prefab, spawnPoint.position, Quaternion.LookRotation(spawnDir.normalized, Vector3.up);
                    newKart = Instantiate(kart, spawnPoint, Quaternion.identity).GetComponent<Kart>();
                    //Player.Insert(0, newKart);

                    //untuk parameter position
                    for(int i = 0; i < parameterPositionPlayer.Count; i++){
                        parameterPositionPlayer[i].maxValue = CP.Count;
                        if (i < Player.Count){
                            parameterPositionPlayer[i].gameObject.SetActive(true);
                        }
                        else{
                            parameterPositionPlayer[i].gameObject.SetActive(false);
                        }
                    }
                }

                if (uiContainer != null)
                {
                    UIControl uiController = uiContainer.GetComponent<UIControl>();
                    if (uiController != null)
                    {
                        uiController.Initialize(newKart);
                    }

                    uiContainer.SetActive(true);
                }

                // Connect the camera to the spawned kart
                if (kartCam != null)
                {
                    kartCam.Initialize(newKart);
                }

                //gameObject.SetActive(false);

                //StartCoroutine(SpawnTime());
            }
            if (MultiplayerScene == true){
                Debug.Log("Multiplayer Scene");
            }
        }

        void Update(){
            if (Input.GetKeyDown("r")){
                PlayerPrefs.DeleteAll();
                Scene s = SceneManager.GetActiveScene();
                SceneManager.LoadScene(s.name);
            }
            GetIndexPosition();
        }

        //untuk parameter position
        public void GetIndexPosition(){
            for(int i = 0; i < Player.Count; i++){
                //CarCPManager CCPM = Player[i].gameObject.GetComponent<CarCPManager>();
                int indexPosition = 0;
                /*if(Player[i].lastCPCollider != null){
                    //indexPosition = CP.IndexOf(Player[i].lastCPCollider);
                    for(int j = 0; j < CP.Count; j++){
                        if(Player[i].lastCPCollider == CP[j]){
                            indexPosition = j;
                            break;
                        }
                }
                }else{
                    indexPosition = 0;
                }*/
                indexPosition = Player[i].cpCrossed;

                parameterPositionPlayer[i].value = indexPosition;
            }
        }

        IEnumerator SpawnTime(){
            yield return new WaitForSeconds(1f);
            //SpawnKart();
        }
    }
}

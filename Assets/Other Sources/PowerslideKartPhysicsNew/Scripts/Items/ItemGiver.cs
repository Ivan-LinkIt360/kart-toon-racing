﻿// Copyright (c) 2021 Justin Couch / JustInvoke
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PowerslideKartPhysics
{
    [DisallowMultipleComponent]
    // Class for objects that give items to karts when touched
    public class ItemGiver : MonoBehaviour
    {
        ItemManager manager;
        Collider trig;
        public GameObject rend;
        public string itemName;
        public int ammo = 1;
        public float cooldown = 1.0f;
        float offTime = 0.0f;

        public ParticleSystem ps;

        private void Awake()
        {
            manager = FindObjectOfType<ItemManager>();
            trig = GetComponent<Collider>();
            rend.GetComponent<Renderer>();
            ps = GetComponent<ParticleSystem>();
            offTime = cooldown;
            
        }

        private void Update()
        {
            if (trig == null || rend == null) { 
                return; 
                
                }

            offTime += Time.deltaTime;

            // Disable trigger and renderer during cooldown
            trig.enabled = rend.GetComponent<Renderer>().enabled = offTime >= cooldown;
            
        }

        private void OnTriggerEnter(Collider other)
        {
            
            if (manager != null)
            {
                // Give item to caster
                ItemCaster caster = other.transform.GetTopmostParentComponent<ItemCaster>();
                ps.Play();
                //Debug.Log("Dapat Item");
                
                if (caster != null)
                {
                    offTime = 0.0f;
                    
                    // Give specific item if named, otherwise random item
                    caster.GiveItem(
                        string.IsNullOrEmpty(itemName) ? manager.GetRandomItem() : manager.GetItem(itemName),
                        ammo, false);
                }
            }
        }
    }
}
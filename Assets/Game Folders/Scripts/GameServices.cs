using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Unity.Services.Core;
using Unity.Services.Authentication;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using Unity.Notifications.Android;
#endif

public class GameServices : MonoBehaviour
{
    //[SerializeField] private GameObject panelLeaderboard;
    //[SerializeField] private GameObject panelHome;

    //[SerializeField] private Button b_closeHighscore;
    //[SerializeField] private Button b_highScore;
    [SerializeField] private string leaderboardID;

    [SerializeField] private string uri = "http://gameson.r3ste.mobi/";

    [SerializeField] private GameObject forceUpdatePage;
    //[SerializeField] private GameObject dailyBonusPage;

    [SerializeField] private GamesonData gamesonServer;

    [SerializeField] private bool successUpdateScore;

    private int jarakWaktu = 0;
    public int GetJarakWaktu() { return jarakWaktu; }

    //[SerializeField] private AuthenticationPage page;

    private async void Awake()
    {
        await UnityServices.InitializeAsync();

        if (AuthenticationService.Instance.IsSignedIn)
        {
            return;
        }
#if UNITY_ANDROID
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.Instance.Authenticate(OnPlayGamesSignedIn);

        AuthenticationService.Instance.SignedIn += Instance_SignedIn;
        AuthenticationService.Instance.SignInFailed += Instance_SignInFailed;
#endif

        StartCoroutine(CheckAppVersion());
    }

    IEnumerator CheckAppVersion()
    {
        UnityWebRequest request = UnityWebRequest.Get(uri);
        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.InProgress:
                break;
            case UnityWebRequest.Result.Success:
                string json = request.downloadHandler.text;
                gamesonServer = JsonUtility.FromJson<GamesonData>(json);

                DataServer findGame = Array.Find(gamesonServer.gameson, g => g.DataName == "Kart Toon Racing");
                if (findGame != null)
                {
                    if (findGame.DataVersion != Application.version)
                    {
                        forceUpdatePage.SetActive(true);
                    }
                    else
                    {
                        forceUpdatePage.SetActive(false);
                    }
                }

                break;
            case UnityWebRequest.Result.ConnectionError:
                break;
            case UnityWebRequest.Result.ProtocolError:
                break;
            case UnityWebRequest.Result.DataProcessingError:
                break;
            default:
                break;
        }

    }

    private void Instance_SignedIn()
    {
        CheckTimeSave();
    }


    private void Instance_SignInFailed(RequestFailedException obj)
    {
        CheckTimeSave();
    }

    public string GetNewVersion()
    {
        return gamesonServer.gameson[2].DataVersion;
    }

    private void Start()
    {
        /*b_highScore.onClick.AddListener(() =>
        {
            //panelLeaderboard.SetActive(true);
            //panelHome.SetActive(false);

            //GetLeaderboard();
#if UNITY_ANDROID
            PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboardID);
#endif
        });*/

        /*b_closeHighscore.onClick.AddListener(() =>
        {
            panelLeaderboard.SetActive(false);
            panelHome.SetActive(true);
        });*/

#if UNITY_ANDROID
        // for notification
        var channel = new AndroidNotificationChannel()
        {
            Id = "channel_id",
            Name = "Default Channel",
            Importance = Importance.Default,
            Description = "Default Notification",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(channel);
        AndroidNotificationCenter.OnNotificationReceived += AndroidNotificationCenter_OnNotificationReceived;

        RequestNotification("Claim Your Bonus Now!", "Dont forget to claim youre daily bonus !", 6);
#endif

#if UNITY_ANDROID
        //b_highScore.gameObject.SetActive(true);
#endif
#if UNITY_IOS     
    //b_highScore.gameObject.SetActive(false);
#endif
        //b_highScore.gameObject.SetActive(PlayerPrefs.HasKey("PEMAINBARU"));

    }

    public void SetHighscoreButton()
    {
        // hapus untuk aktivasi leaderboard kembali
        //b_highScore.gameObject.SetActive(true);
    }
#if UNITY_IOS
    public void SetNewLeaderboard(int value)
    {
        //return value here
    }
#endif

#if UNITY_ANDROID
    private void OnDisable()
    {

        AndroidNotificationCenter.OnNotificationReceived -= AndroidNotificationCenter_OnNotificationReceived;

    }

    private void AndroidNotificationCenter_OnNotificationReceived(AndroidNotificationIntentData data)
    {
        AndroidNotificationCenter.CancelAllNotifications();
    }

    public void SetNewLeaderboard(int newScore)
    {
        //PushLeaderboard(newScore);
        PlayGamesPlatform.Instance.ReportScore(newScore, leaderboardID, (bool success) => { successUpdateScore = success; });
    }


#endif
    public async void ChangeName(string newName)
    {
        //AdsManager.Instance.RequestRewardedAds(AdsManager.ChangeName);
        await AuthenticationService.Instance.UpdatePlayerNameAsync(newName);

        if (newName == "IrvanNuariNugroho")
        {
            //GameManager.Instance.MoreCoin(20000);
        }
    }

    private void CheckTimeSave()
    {
        string waktukemarin = DateTime.Now.AddDays(-1).ToShortDateString();

        if (PlayerPrefs.HasKey("CALENDAR"))
        {
            string waktuSave = PlayerPrefs.GetString("CALENDAR");

            DateTime t = DateTime.Parse(waktuSave);
            jarakWaktu = DateTime.Now.Day - t.Day;

            if (PlayerPrefs.HasKey("CLAIMBONUS"))
            {
                if (PlayerPrefs.GetInt("CLAIMBONUS") == jarakWaktu)
                {
                    //dailyBonusPage.SetActive(false);
                }
                else
                {
                    //dailyBonusPage.SetActive(true);
                }
            }
        }
        else
        {
            //dailyBonusPage.SetActive(true);
            PlayerPrefs.SetString("CALENDAR", waktukemarin);
            DateTime temp = DateTime.Now.AddDays(-1);
            jarakWaktu = DateTime.Now.Day - temp.Day;
        }
    }

    public void RequestNotification(string judul, string infoText, int fireTime)
    {
        StartCoroutine(RequestNotificationPermission(judul, infoText, fireTime));
    }

#if UNITY_IOS
    IEnumerator RequestNotificationPermission(string judul, string infoText, int fireTime)
    {
        yield return null;
    }
#endif

#if UNITY_ANDROID
    IEnumerator RequestNotificationPermission(string judul, string infoText, int fireTime)
    {
        var request = new PermissionRequest();

        while (request.Status == PermissionStatus.RequestPending)
            yield return null;

        // here use request.Status to determine users response
        switch (request.Status)
        {
            case PermissionStatus.NotRequested:
                break;
            case PermissionStatus.Allowed:
                CreateNotification(judul, infoText, fireTime);
                break;
            case PermissionStatus.Denied:
                break;
            case PermissionStatus.DeniedDontAskAgain:
                break;
            case PermissionStatus.RequestPending:
                break;
        }
    }

    private void CreateNotification(string judul, string infoText, int fireTime)
    {
        var notification = new AndroidNotification();

        notification.Title = judul;
        notification.Text = infoText;
        notification.FireTime = DateTime.Now.AddHours(fireTime);

        AndroidNotificationCenter.SendNotification(notification, "channel_id");
        AndroidNotificationCenter.CancelAllNotifications();
    }

    private async void OnPlayGamesSignedIn(SignInStatus status)
    {
        switch (status)
        {
            case SignInStatus.Success:
                //page.SetLabel("Signed In . .");
                PlayGamesPlatform.Instance.RequestServerSideAccess(true, OnGetCallbacks);
                //b_highScore.gameObject.SetActive(true);
                break;
            case SignInStatus.InternalError:
                //page.SetLabel("Authentication failed. .");
                await SignInAnonymouslyAsync();
                //b_highScore.gameObject.SetActive(false);
                break;
            case SignInStatus.Canceled:
                //page.SetLabel("Authentication canceled. .");
                await SignInAnonymouslyAsync();
                //b_highScore.gameObject.SetActive(false);
                break;
        }
    }

    private async void OnGetCallbacks(string token)
    {
        await SignInWithGooglePlayGamesAsync(token);
    }

    async Task SignInWithGooglePlayGamesAsync(string authCode)
    {
        try
        {
            await AuthenticationService.Instance.SignInWithGooglePlayGamesAsync(authCode);
        }
        catch (AuthenticationException ex)
        {
            // Compare error code to AuthenticationErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
        catch (RequestFailedException ex)
        {
            // Compare error code to CommonErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
    }
#endif

    async Task SignInAnonymouslyAsync()
    {
        try
        {
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
            //page.SetLabel("Sign in anonymously succeeded!");

            // Shows how to get the playerID
            Debug.Log($"PlayerID: {AuthenticationService.Instance.PlayerId}");

        }
        catch (AuthenticationException ex)
        {
            // Compare error code to AuthenticationErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
        catch (RequestFailedException ex)
        {
            // Compare error code to CommonErrorCodes
            // Notify the player with the proper error message
            Debug.LogException(ex);
        }
    }
}





[System.Serializable]
public class DataServer
{
    public string DataName;
    public string DataVersion;
    public string Url;
}

[System.Serializable]
public class GamesonData
{
    public DataServer[] gameson;
}

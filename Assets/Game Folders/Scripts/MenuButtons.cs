using UnityEngine;
using UnityEngine.UI;

namespace Gameson
{
    public class MenuButtons : MonoBehaviour
    {
        [SerializeField] private Button b_menu,b_store,b_coinStore,b_kart,b_event;

        private void Start()
        {
            b_menu.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.MainMenu));
            b_store.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Shop));
            b_coinStore.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.CoinShop));
            b_kart.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Kart));
            b_event.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Event));
        }
    }
}
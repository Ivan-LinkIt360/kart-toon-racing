using Gameson;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MenuStats : MonoBehaviour
{
    [SerializeField] private TMP_Text label_coin;
    [SerializeField] private TMP_Text label_blueCoin;
    [SerializeField] private TMP_Text label_diamond;
    [SerializeField] private TMP_Text label_playerName;

    private void Start()
    {
        GameManager.Instance.OnCurrenciesChange += Instance_OnCurrenciesChange;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnCurrenciesChange -= Instance_OnCurrenciesChange;
    }

    private void Instance_OnCurrenciesChange(int coin, int gem, int coinBiru)
    {
        label_coin.text = $"{coin}";
        label_blueCoin.text = $"{coinBiru}";
        label_diamond.text = $"{gem}";
    }


    
}

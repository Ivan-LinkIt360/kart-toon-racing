﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Gameson
{
    public class MainMenuPage : Page
    {
        [SerializeField] TrackClass[] tracks;
        [SerializeField] GameObject[] trackImage;

        [SerializeField] Image selectedTrack;
        [SerializeField] Sprite defaultImage;
        [SerializeField] GameObject connectingPanel;

        [SerializeField] private Button b_singlePlayer;
        [SerializeField] private Button b_multiPlayer;
        [SerializeField] private Button b_playGame;

        private void Start()
        {
            b_multiPlayer.interactable = false;

            b_playGame.onClick.AddListener(() => 
            {
                AdsManager.Instance.RequestInterstitialAds();
                ChangeScene("KartTrack"); 
            });
            b_singlePlayer.onClick.AddListener(SinglePlayer);
            b_multiPlayer.onClick.AddListener(MultiPlayer);
            RefreshUI();
        }

        #region MultiPlayer Photon
        public void MultiPlayer()
        {
            PhotonNetwork.ConnectUsingSettings();
            //connectingPanel.SetActive(true);
            GameManager.Instance.ChangeState(GameState.Lobby);
        }

        public override void OnConnectedToMaster()
        {
            b_multiPlayer.interactable = true;
            PhotonNetwork.JoinLobby();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            b_multiPlayer.interactable = false;
        }
        public override void OnJoinedLobby()
        {

            //GameManager.Instance.ChangeState(GameState.Lobby);
            //SceneManager.LoadScene("MultiplayerScene");
        }
        #endregion

        private void SinglePlayer()
        {
            TrackClass t = GameManager.Instance.GetTrack();
            if (t != null)
            {
                AdsManager.Instance.RequestInterstitialAds();
                ChangeScene(t.nama);
            }
        }

        void RefreshUI()
        {
            TrackClass t = GameManager.Instance.GetTrack();
            if (t != null)
            {
                selectedTrack.sprite = t.trackImage;
            }
            else
                selectedTrack.sprite = tracks[0].trackImage;
            for (int i = 0; i < trackImage.Length; i++)
            {
                try
                {
                    trackImage[i].transform.GetChild(0).GetComponent<Image>().sprite = tracks[i].trackImage; //trackimage = 6
                    if(tracks[i].isUnlocked)
                    {
                        trackImage[i].transform.GetChild(1).gameObject.SetActive(false);
                    }
                    trackImage[i].transform.GetChild(2).GetComponent<Text>().text = tracks[i].nama;
                    connectingPanel.SetActive(false);
                }
                catch
                {
                    trackImage[0].transform.GetChild(0).GetComponent<Image>().sprite = defaultImage;
                    trackImage[i].transform.GetChild(1).gameObject.SetActive(true);
                    trackImage[i].transform.GetChild(2).GetComponent<Text>().text = "Coming Soon";
                    //Debug.Log("connectingPanel ga bisa ditutup");
                }
            }
        }
    }

}

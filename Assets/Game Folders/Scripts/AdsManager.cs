using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameson
{
    public class AdsManager : MonoBehaviour
    {
        public static AdsManager Instance;

        [SerializeField] private string interstitial_id;
        [SerializeField] private string rewarded_id;

        private InterstitialAd interstitial_ads;
        private RewardedAd rewarded_ads;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            MobileAds.Initialize((initStatus) =>
            {
                Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
                foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
                {
                    string className = keyValuePair.Key;
                    AdapterStatus status = keyValuePair.Value;
                    switch (status.InitializationState)
                    {
                        case AdapterState.NotReady:
                        // The adapter initialization did not complete.
                        Debug.Log("Adapter: " + className + " not ready.");
                            break;
                        case AdapterState.Ready:
                        // The adapter was successfully initialized.
                        Debug.Log("Adapter: " + className + " is initialized.");
                            break;
                    }
                }
            });

            LoadInterstitialAds();
            LoadRewardedAds();
        }

        public void RequestRewardedAds()
        {
            rewarded_ads.Show(OnGetRewardSuccess);
        }

        private void LoadRewardedAds()
        {
            if (rewarded_ads != null)
            {
                rewarded_ads.Destroy();
                rewarded_ads = null;
            }

            Debug.Log("Loading the rewarded ad.");

            // create our request used to load the ad.
            var adRequest = new AdRequest();
            adRequest.Keywords.Add("unity-admob-sample");

            // send the request to load the ad.
            RewardedAd.Load(rewarded_id, adRequest,
                (RewardedAd ad, LoadAdError error) =>
                {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                    {
                        Debug.LogError("Rewarded ad failed to load an ad " +
                                       "with error : " + error);
                        return;
                    }

                    Debug.Log("Rewarded ad loaded with response : "
                              + ad.GetResponseInfo());

                    rewarded_ads = ad;
                });
        }

        private void OnGetRewardSuccess(Reward newReward)
        {
            // get reward code here
        }

        public void RequestInterstitialAds()
        {
            interstitial_ads.Show();
        }

        private void LoadInterstitialAds()
        {
            if (interstitial_ads != null)
            {
                interstitial_ads.Destroy();
                interstitial_ads = null;
            }

            var adRequest = new AdRequest();
            adRequest.Keywords.Add("interstitial-ads-ID");

            // send the request to load the ad.
            InterstitialAd.Load(interstitial_id, adRequest,
                (InterstitialAd ad, LoadAdError error) =>
                {
                // if error is not null, the load request failed.
                if (error != null || ad == null)
                    {
                        Debug.LogError("interstitial ad failed to load an ad " +
                                       "with error : " + error);
                        return;
                    }

                    Debug.Log("Interstitial ad loaded with response : "
                              + ad.GetResponseInfo());

                    interstitial_ads = ad;
                });
        }
    }
}
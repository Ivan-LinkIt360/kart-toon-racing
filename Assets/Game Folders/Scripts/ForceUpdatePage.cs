using Gameson;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ForceUpdatePage : MonoBehaviour
{
    [SerializeField, TextArea(3, 3)] private string uri;

    [SerializeField] private TMP_Text label_newVersion, label_oldVersion;

    [SerializeField] private Button b_update;

    private void Start()
    {
        b_update.onClick.AddListener(() =>
        {
            Application.OpenURL(uri);
        });


        StartCoroutine(CheckVersion());
    }

    IEnumerator CheckVersion()
    {
        yield return new WaitForSeconds(2f);
        if (!string.IsNullOrEmpty(GameManager.Instance.GetGameServices().GetNewVersion()))
        {
            label_oldVersion.text = $"Your current game Version {Application.version} is behind !";
            label_newVersion.text = $"update to version : {GameManager.Instance.GetGameServices().GetNewVersion()}";
        }
    }
}
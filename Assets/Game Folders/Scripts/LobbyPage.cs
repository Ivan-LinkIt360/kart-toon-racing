using Gameson;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPage : Page
{
    [SerializeField] private GameObject panel_loading;
    [SerializeField] private GameObject panel_joinRandom;
    [SerializeField] private GameObject panel_createRoom;

    [SerializeField] private TMP_InputField input_roomName;

    [SerializeField] private Button b_join, b_create;

    [SerializeField] private Dictionary<string, RoomInfo> cachedRoomList = new Dictionary<string, RoomInfo>();

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.JoinLobby();
        
        panel_loading.SetActive(true);

        b_join.onClick.AddListener(JoinRandomRoom);
        b_create.onClick.AddListener(CreateRoom);
    }

    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    private void CreateRoom()
    {
        if(string.IsNullOrEmpty(input_roomName.text))
        {
            return;
        }
        PhotonNetwork.CreateRoom(input_roomName.text);
    }

    private void JoinRandomRoom()
    {
        Debug.Log("Join random room");
    }

    public override void OnJoinedRoom()
    {
        panel_loading.SetActive(false);
        panel_joinRandom.SetActive(true);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Create random room");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("failed Join random room");
        panel_loading.SetActive(false);
        panel_createRoom.SetActive(true);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("failed Join random room");
        panel_loading.SetActive(false);
        panel_createRoom.SetActive(true);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        UpdateCachedRoomList(roomList);
    }

    private void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            RoomInfo info = roomList[i];
            if (info.RemovedFromList)
            {
                cachedRoomList.Remove(info.Name);
            }
            else
            {
                cachedRoomList[info.Name] = info;
            }
        }

       // panel_loading.SetActive(false);
    }

    public override void OnLeftLobby()
    {
        cachedRoomList.Clear();
    }
}

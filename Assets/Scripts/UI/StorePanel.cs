using Gameson;
using KartToon.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorePanel : Page
{
    [Header("Free Prizes")]
    [SerializeField] private Button b_Spin;

    private void Start()
    {
        b_Spin.onClick.AddListener(() =>
        {
            AdsManager.Instance.RequestRewardedAds();
        });
    }
}

namespace Gameson
{
    public enum PageName
    {
        Loading,
        MainMenu,
        Singleplayer,
        Multiplayer,
        GamePaused,
        GameUnpaused,
        CoinShop,
        Shop,
        Kart,
        Event,
        SelectLevel,
        Profile,
        Lobby,
        RoomList
    }
}
using KartToon.UI;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameson
{
    public class Page : MonoBehaviourPunCallbacks
    {
        [SerializeField] public PageName pageName;

        protected virtual void ChangeScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }

}

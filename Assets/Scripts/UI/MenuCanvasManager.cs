﻿using UnityEngine;

namespace Gameson
{
    public class MenuCanvasManager : CanvasManager
    {
        [SerializeField] private RotatorCamera rotateCamera;

        private void Start()
        {
            GameManager.Instance.OnStateChange += Instance_OnStateChange;

            GameManager.Instance.GetCurrenciesData();
        }

        private void Instance_OnStateChange(GameState newState)
        {
            switch (newState)
            {
                case GameState.MainMenu:
                    StartCoroutine(SetPage(PageName.MainMenu));
                    rotateCamera.SelectTrack();
                    break;
                case GameState.Setting:
                    break;
                case GameState.Profile:
                    StartCoroutine(SetPage(PageName.Profile));
                    break;
                case GameState.Loading:
                    StartCoroutine(SetPage(PageName.Loading));
                    break;
                case GameState.CoinShop:
                    StartCoroutine(SetPage(PageName.CoinShop));
                    rotateCamera.SelectCoin();
                    break;
                case GameState.Shop:
                    StartCoroutine(SetPage(PageName.Shop));
                    rotateCamera.SelectShop();
                    break;
                case GameState.Kart:
                    StartCoroutine(SetPage(PageName.Kart));
                    rotateCamera.SelectBase();
                    break;
                case GameState.Singleplayer:
                    break;
                case GameState.Multiplayer:
                    break;
                case GameState.Event:
                    StartCoroutine(SetPage(PageName.Event));
                    rotateCamera.SelectEvent();
                    break;
                case GameState.Lobby:
                    StartCoroutine(SetPage(PageName.Lobby));
                    break;
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using Gameson;

namespace KartToon.UI
{
    public class CanvasManagerMultiplayer : MonoBehaviourPunCallbacks
    {
        [SerializeField] protected Page[] allPages;

        public override void OnEnable()
        {
            base.OnEnable();

            GameManager.Instance.OnStateChange += Instance_OnStateChange1;
        }

        private void Instance_OnStateChange1(GameState newState)
        {
            switch (newState)
            {
                case GameState.Lobby:
                    SetPage(PageName.Lobby);
                    break;
                case GameState.RoomList:
                    SetPage(PageName.RoomList);
                    break;
            }
        }

        public virtual void SetPage(PageName pageName)
        {
            foreach (var page in allPages)
            {
                page.gameObject.SetActive(false);
            }
            Page currentPage = Array.Find(allPages, p => p.pageName == pageName);
            currentPage?.gameObject.SetActive(true);

        }
        private void Join(string roomName)
        {
            Room room = new Room(roomName, null);
            PhotonNetwork.JoinRoom(roomName);
        }
        public override void OnJoinedRoom()
        {
            foreach (Player player in PhotonNetwork.PlayerList)
            {
                if (player.ActorNumber != PhotonNetwork.LocalPlayer.ActorNumber)
                {
                    Test(player);
                }
            }

            // Instantiate player prefab for local player
            Test(PhotonNetwork.LocalPlayer);
        }

        private void Test(Player player)
        {
            Debug.Log((string)player.CustomProperties["PlayerPrefab"]);
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("Tidak bisa masuk karena" + message);
        }
        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            for (int i = 0; i < 99; i++)
            {
                //try
                //{
                //    content.transform.GetChild(i);
                //    content.transform.GetChild(i).GetComponent<Button>().onClick.AddListener(() => Join(content.transform.GetChild(i).name));
                //    content.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = string.Format("{0} ({1}/{2})", roomList[i].Name, roomList[i].PlayerCount, roomList[i].MaxPlayers);
                //    bool a = roomList[i].IsOpen;
                //}
                //catch
                //{
                //    Debug.Log("nothing");
                //    Debug.Log("Ada masalah pada room");
                //    content.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = " ";
                //    content.transform.GetChild(i).GetComponent<Button>().interactable = false;
                //}

            }

        }

    }
}

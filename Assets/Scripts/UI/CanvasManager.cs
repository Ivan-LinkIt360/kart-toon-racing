using KartToon.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Gameson
{
    public class CanvasManager : MonoBehaviour
    {
        [SerializeField] protected Page[] allPages;

        public virtual IEnumerator SetPage(PageName pageName)
        {
            foreach (var page in allPages)
            {
                page.gameObject.SetActive(false);
            }
            Page currentPage = Array.Find(allPages, p => p.pageName == pageName);

            yield return new WaitForSeconds(0.6f);

            currentPage?.gameObject.SetActive(true);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KartPage : MonoBehaviour
{
    [SerializeField] private Button b_next, b_prev;
    [SerializeField] private Image[] slider_stats;

    public void SetStats(float a , float b , float c , float d)
    {
        slider_stats[0].fillAmount = a / 100f;
        slider_stats[1].fillAmount = b / 100f;
        slider_stats[2].fillAmount = c / 100f;
        slider_stats[3].fillAmount = d / 100f;
    }
}

﻿namespace Gameson
{
    public enum GameState
    {
        MainMenu,
        Setting,
        Profile,
        Loading,
        CoinShop,
        Shop,
        Kart,
        Singleplayer,
        Multiplayer,
        Lobby,
        RoomList,
        Event
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewTrack : MonoBehaviour
{
    public GameObject warningLockedTrack;

    public GameObject[] listViewTrack;

    public int i;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void viewTrack(int index){
        ResetlistViewTrack();
        listViewTrack[index].SetActive(true);
    }

    public void ResetlistViewTrack()
    {
        for(i = 0; i < listViewTrack.Length; i++){
            if(listViewTrack[i].activeSelf) listViewTrack[i].SetActive(false);
            
        }
    }

    public void OpenWarningLocked(){
        warningLockedTrack.SetActive(true);
        StartCoroutine(NonAktifWarning());
    }

    IEnumerator NonAktifWarning(){
        yield return new WaitForSeconds(2f);
        warningLockedTrack.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollect : MonoBehaviour
{
    public int coinBlue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider Col){
        if (Col.gameObject.CompareTag("CoinBlue")){
            coinBlue = coinBlue + 1;
            Col.GetComponent<Collider>().enabled = false;
            PlayerPrefs.SetInt("LastCoinBlue", coinBlue);
            Destroy(Col.gameObject, 1f);
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CoinBlue : MonoBehaviour
{
    public Animator Anim;
    public GameObject sfx;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider Col){
        if (Col.gameObject.CompareTag("PlayerTrigger")){
            //Debug.Log("Tabrak Coin Blue");
            Anim.SetTrigger("CoinBlue");
            sfx.GetComponent<AudioSource>().Play();
        }
    }
}

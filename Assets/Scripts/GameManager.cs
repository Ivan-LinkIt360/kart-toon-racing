﻿using UnityEngine;

namespace Gameson
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        [SerializeField] private GameState currentState = GameState.MainMenu;
        [SerializeField] private PlayerData playerData;

        [SerializeField] private CharacterClass character;
        private TrackClass track;

        public delegate void OnCurrenciesValueChangeDelegate(int coin, int gem,int coinBiru);
        public event OnCurrenciesValueChangeDelegate OnCurrenciesChange;

        public delegate void StateChangeDelegate(GameState newState);
        public event StateChangeDelegate OnStateChange;

        private GameServices gameServices;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);

            gameServices = GetComponent<GameServices>();
        }

        public GameServices GetGameServices()
        {
            return gameServices;
        }

        public void ChangeState(GameState newState)
        {
            currentState = newState;
            OnStateChange(currentState);
        }

        public void GetCurrenciesData()
        {
            OnCurrenciesChange?.Invoke(playerData.coin, playerData.diamond, playerData.blueCoin);
        }

        public void AddCurrenciesValue(int coin, int gem,int coinBiru)
        {
            playerData.coin += coin;
            playerData.diamond += gem;
            playerData.blueCoin += coinBiru;
        }

        public void setupCharacter(CharacterClass _character)
        {
            character = _character;
        }
        public CharacterClass GetCharacter()
        {
            return character;
        }
        public void SetTrack(TrackClass _track)
        {
            track = _track;
        }
        public TrackClass GetTrack()
        {
            return track;
        }
    }

    [System.Serializable]
    public class PlayerData
    {
        public string playerName;

        public int coin;
        public int blueCoin;
        public int diamond;
    }
}
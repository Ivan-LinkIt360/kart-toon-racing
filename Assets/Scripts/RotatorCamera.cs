﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotatorCamera : MonoBehaviour
{
    public Animator Anim;

    private int angka;

    void Start()
    {
        Anim = GetComponent<Animator>();
        angka = 4;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectBase(){
        
        
        if (angka == 4){
            Anim.SetTrigger("RaceToBase");
        }
        if (angka == 2){
            Anim.SetTrigger("ShopToBase");
        }
        if (angka == 5){
            Anim.SetTrigger("EventToBase");
        }
        if (angka == 1){
            Anim.SetTrigger("CoinToBase");
        }
        angka = 3;
    }

    public void SelectTrack(){
        
        

        if (angka == 3){
            Anim.SetTrigger("BaseToRace");
        }
        if (angka == 2){
            Anim.SetTrigger("ShopToRace");
        }
        if (angka == 5){
            Anim.SetTrigger("EventToRace");
        }
        if (angka == 1){
            Anim.SetTrigger("CoinToRace");
        }
        angka = 4;
    }

    public void SelectShop(){
        
        

        if (angka == 3){
            Anim.SetTrigger("BaseToShop");
        }
        if (angka == 4){
            Anim.SetTrigger("RaceToShop");
        }
        if (angka == 5){
            Anim.SetTrigger("EventToShop");
        }
        if (angka == 1){
            Anim.SetTrigger("CoinToShop");
        }
        angka = 2;
    }

    public void SelectEvent(){
        
        

        if (angka == 4){
            Anim.SetTrigger("RaceToEvent");
        }
        if (angka == 2){
            Anim.SetTrigger("ShopToEvent");
        }
        if (angka == 3){
            Anim.SetTrigger("BaseToEvent");
        }
        if (angka == 1){
            Anim.SetTrigger("CoinToEvent");
        }
        angka = 5;
    }

    public void SelectCoin(){
        
        

        if (angka == 4){
            Anim.SetTrigger("RaceToCoin");
        }
        if (angka == 2){
            Anim.SetTrigger("ShopToCoin");
        }
        if (angka == 3){
            Anim.SetTrigger("BaseToCoin");
        }
        if (angka == 5){
            Anim.SetTrigger("EventToCoin");
        }
        angka = 1;
    }

    public void SelectKartOpenAnim(){
        
        Anim.SetTrigger("SelectKartOpen");
    }
    public void SelectKartExit(){
        
        Anim.SetTrigger("SelectKartExit");
    }
}

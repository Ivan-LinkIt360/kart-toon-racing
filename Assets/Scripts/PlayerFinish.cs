﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerFinish : MonoBehaviourPun
{
    public int JuaraBerapa, LastCoin, LastXP;
    public GameObject Bintang1, Bintang2, Bintang3, BintangDummy;
    public GameObject TextJuara1, TextJuara2, TextJuara3, TextJuara4;
    public string PlayerName, OpponentName;
    public Text TextLastCoin, TextLastXP;

    public bool isPlayer;
    public bool PlayerMultiplayer;
    public int coinJuara1, coinJuara2, coinJuara3;
    public int XpJuara1, XpJuara2, XpJuara3;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DisableBintang());   
        //LastCoin = PlayerPrefs.GetInt("Coin");
        PlayerName = PlayerPrefs.GetString("PlayerName", PlayerName);
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerMultiplayer == false)
        {
            if ((PlayerName == PhotonNetwork.LocalPlayer.NickName)/* || (PlayerName == "Player 2" ) || (PlayerName == "Player 3" ) || (PlayerName == "Player 4" )*/){
                //TextLastCoin = GameObject.Find("TextLastCoinPlayer").GetComponent<Text>();
                //TextLastXP = GameObject.Find("TextLastXPPlayer").GetComponent<Text>();
                if (isPlayer == true){
                    Bintang1 = GameObject.FindGameObjectWithTag("Star1");
                    Bintang2 = GameObject.FindGameObjectWithTag("Star2");
                    Bintang3 = GameObject.FindGameObjectWithTag("Star3");

                    BintangDummy = GameObject.FindGameObjectWithTag("StarDummy");

                    TextLastCoin = GameObject.Find("TextLastCoinPlayer").GetComponent<Text>();
                    TextLastXP = GameObject.Find("TextLastXPPlayer").GetComponent<Text>();
                }
                if (isPlayer == false){
                    TextLastCoin = GameObject.Find("TextScoreDummy").GetComponent<Text>();
                    TextLastXP = GameObject.Find("TextXPDummy").GetComponent<Text>();
                }
                
            }

            TextJuara1 = GameObject.FindGameObjectWithTag("TextJuara1");
            TextJuara2 = GameObject.FindGameObjectWithTag("TextJuara2");
            TextJuara3 = GameObject.FindGameObjectWithTag("TextJuara3");
            TextJuara4 = GameObject.FindGameObjectWithTag("TextJuara4");

            
            

            JuaraBerapa = PlayerPrefs.GetInt("JuaraBerapa");
            if (Input.GetKeyDown("r"))
            {
                PlayerPrefs.DeleteAll();
                //Application.LoadLevel("Demo");
            }

            if (Input.GetKeyDown("="))
            {
                LastCoin += 100;
            }

            if (Input.GetKeyDown("f"))
            {
                
            }

            PlayerPrefs.SetInt("LastCoin", LastCoin);
            PlayerPrefs.SetInt("LastXP", LastXP);

            if (LastCoin == coinJuara1)
            {
                /*Bintang1.GetComponent<Image>().enabled = true;
                Bintang2.GetComponent<Image>().enabled = true;
                Bintang3.GetComponent<Image>().enabled = true;*/
                PlayerPrefs.SetInt("LastXP", LastXP);
                TextLastXP.text = PlayerPrefs.GetInt("LastXP", 0).ToString();
            }
            if (LastCoin == coinJuara2){
                /*Bintang1.GetComponent<Image>().enabled = true;
                Bintang2.GetComponent<Image>().enabled = true;*/
                PlayerPrefs.SetInt("LastXP", LastXP);
                TextLastXP.text = PlayerPrefs.GetInt("LastXP", 0).ToString();
            }
            if (LastCoin == coinJuara3){
                //Bintang1.GetComponent<Image>().enabled = true;
                PlayerPrefs.SetInt("LastXP", LastXP);
                TextLastXP.text = PlayerPrefs.GetInt("LastXP", 0).ToString();
            }
        }
        


        if (PlayerMultiplayer == true){
            if(photonView.IsMine)/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Multiplayer
            if ((PlayerName == PhotonNetwork.LocalPlayer.NickName)/* || (PlayerName == "Player 2" ) || (PlayerName == "Player 3" ) || (PlayerName == "Player 4" )*/){
                //TextLastCoin = GameObject.Find("TextLastCoinPlayer").GetComponent<Text>();
                //TextLastXP = GameObject.Find("TextLastXPPlayer").GetComponent<Text>();
                if (isPlayer == true){
                    Bintang1 = GameObject.FindGameObjectWithTag("Star1");
                    Bintang2 = GameObject.FindGameObjectWithTag("Star2");
                    Bintang3 = GameObject.FindGameObjectWithTag("Star3");

                    BintangDummy = GameObject.FindGameObjectWithTag("StarDummy");

                    TextLastCoin = GameObject.Find("TextLastCoinPlayer").GetComponent<Text>();
                    TextLastXP = GameObject.Find("TextLastXPPlayer").GetComponent<Text>();
                }
                if (isPlayer == false){
                    TextLastCoin = GameObject.Find("TextScoreDummy").GetComponent<Text>();
                    TextLastXP = GameObject.Find("TextXPDummy").GetComponent<Text>();
                }
                
            }

            TextJuara1 = GameObject.FindGameObjectWithTag("TextJuara1");
            TextJuara2 = GameObject.FindGameObjectWithTag("TextJuara2");
            TextJuara3 = GameObject.FindGameObjectWithTag("TextJuara3");
            TextJuara4 = GameObject.FindGameObjectWithTag("TextJuara4");

            
            

            JuaraBerapa = PlayerPrefs.GetInt("JuaraBerapa");
            if (Input.GetKeyDown("r"))
            {
                PlayerPrefs.DeleteAll();
                //Application.LoadLevel("Demo");
            }

            if (Input.GetKeyDown("="))
            {
                LastCoin += 100;
            }

            if (Input.GetKeyDown("f"))
            {
                
            }

            PlayerPrefs.SetInt("LastCoin", LastCoin);
            PlayerPrefs.SetInt("LastXP", LastXP);

            if (LastCoin == coinJuara1){
                Bintang1.GetComponent<Image>().enabled = true;
                Bintang2.GetComponent<Image>().enabled = true;
                Bintang3.GetComponent<Image>().enabled = true;
                PlayerPrefs.SetInt("LastXP", LastXP);
                TextLastXP.text = PlayerPrefs.GetInt("LastXP", 0).ToString();
            }
            if (LastCoin == coinJuara2){
                Bintang1.GetComponent<Image>().enabled = true;
                Bintang2.GetComponent<Image>().enabled = true;
                PlayerPrefs.SetInt("LastXP", LastXP);
                TextLastXP.text = PlayerPrefs.GetInt("LastXP", 0).ToString();
            }
            if (LastCoin == coinJuara3){
                Bintang1.GetComponent<Image>().enabled = true;
                PlayerPrefs.SetInt("LastXP", LastXP);
                TextLastXP.text = PlayerPrefs.GetInt("LastXP", 0).ToString();
            }
        }
    }


    void OnTriggerEnter(Collider col){
        if (PlayerMultiplayer == false){
            if (col.gameObject.tag == "FinishLine") {
                    JuaraBerapa++;
                    PlayerPrefs.SetInt("JuaraBerapa", JuaraBerapa);

                    if (JuaraBerapa == 1)
                    {
                        //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                        if (isPlayer == true)
                        {
                            TextJuara1.GetComponent<Text>().text = PlayerName;
                        }
                        if (isPlayer == false)
                        {
                            TextJuara1.GetComponent<Text>().text = OpponentName;
                        }
                        LastCoin = coinJuara1;
                        LastXP += XpJuara1;
                        PlayerPrefs.SetInt("LastCoin", LastCoin);
                        TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                    }
                    if (JuaraBerapa == 2)
                    {
                        //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                        if (isPlayer == true)
                        {
                            TextJuara2.GetComponent<Text>().text = PlayerName;
                        }
                        if (isPlayer == false)
                        {
                            TextJuara2.GetComponent<Text>().text = OpponentName;
                        }
                        LastCoin = coinJuara2;
                        LastXP += XpJuara2;
                        PlayerPrefs.SetInt("LastCoin", LastCoin);
                        TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                    }
                    if (JuaraBerapa == 3)
                    {
                        //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                        if (isPlayer == true)
                        {
                            TextJuara3.GetComponent<Text>().text = PlayerName;
                        }
                        if (isPlayer == false)
                        {
                            TextJuara3.GetComponent<Text>().text = OpponentName;
                        }
                        LastCoin = coinJuara3;
                        LastXP += XpJuara3;
                        PlayerPrefs.SetInt("LastCoin", LastCoin);
                        TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                    }
                    if (JuaraBerapa == 4)
                    {
                        //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                        if (isPlayer == true)
                        {
                            TextJuara4.GetComponent<Text>().text = PlayerName;
                        }
                        if (isPlayer == false)
                        {
                            TextJuara4.GetComponent<Text>().text = OpponentName;
                        }
                        LastCoin = 0;

                        PlayerPrefs.SetInt("LastCoin", LastCoin);
                        TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                    
                }
            }
        }
        if (PlayerMultiplayer == true){
            if(photonView.IsMine)/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Multiplayer
            if (col.gameObject.tag == "FinishLine"){
                JuaraBerapa++;
                PlayerPrefs.SetInt("JuaraBerapa", JuaraBerapa);

                if (JuaraBerapa == 1){
                    //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                    if (isPlayer == true){
                        TextJuara1.GetComponent<Text>().text = PlayerName;
                    }
                    if (isPlayer == false){
                        TextJuara1.GetComponent<Text>().text = OpponentName;
                    }
                    LastCoin = coinJuara1;
                    LastXP += XpJuara1;
                    PlayerPrefs.SetInt("LastCoin", LastCoin);
                    TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                }
                if (JuaraBerapa == 2){
                    //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                    if (isPlayer == true){
                        TextJuara2.GetComponent<Text>().text = PlayerName;
                    }
                    if (isPlayer == false){
                        TextJuara2.GetComponent<Text>().text = OpponentName;
                    }
                    LastCoin = coinJuara2;
                    LastXP += XpJuara2;
                    PlayerPrefs.SetInt("LastCoin", LastCoin);
                    TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                }
                if (JuaraBerapa == 3){
                    //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                    if (isPlayer == true){
                        TextJuara3.GetComponent<Text>().text = PlayerName;
                    }
                    if (isPlayer == false){
                        TextJuara3.GetComponent<Text>().text = OpponentName;
                    }
                    LastCoin = coinJuara1;
                    LastXP += XpJuara1;
                    PlayerPrefs.SetInt("LastCoin", LastCoin);
                    TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                }
                if (JuaraBerapa == 4){
                    //PlayerName = PlayerPrefs.GetString("Name", PlayerName);
                    if (isPlayer == true){
                        TextJuara4.GetComponent<Text>().text = PlayerName;
                    }
                    if (isPlayer == false){
                        TextJuara4.GetComponent<Text>().text = OpponentName;
                    }
                    LastCoin = 0;
                    
                    PlayerPrefs.SetInt("LastCoin", LastCoin);
                    TextLastCoin.text = PlayerPrefs.GetInt("LastCoin", 0).ToString();
                }
            }
        }
    }

    IEnumerator DisableBintang(){
        if (PlayerMultiplayer == false){
            yield return new WaitForSeconds(1f);
           /* Bintang1.GetComponent<Image>().enabled = false;
            Bintang2.GetComponent<Image>().enabled = false;
            Bintang3.GetComponent<Image>().enabled = false;*/
            JuaraBerapa = 0;
        }
        if (PlayerMultiplayer == true){
            if(photonView.IsMine)/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////Multiplayer
            yield return new WaitForSeconds(1f);
            /*Bintang1.GetComponent<Image>().enabled = false;
            Bintang2.GetComponent<Image>().enabled = false;
            Bintang3.GetComponent<Image>().enabled = false;*/
            JuaraBerapa = 0;
        }
    }
}

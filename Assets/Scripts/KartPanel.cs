﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Gameson;
using TMPro;

namespace KartToon.UI
{
	public class KartPanel : Page
	{
		[SerializeField] private int index = 0;

		[SerializeField] Button nextBTN, prevBTN, selectBTN;
		[SerializeField] GameObject warningPanel,lockPanel,prefabParent;
		
		[SerializeField] Image[] statSlider;

		void Start()
		{
			#region starter
			for (int i = 0; i < KartSlotHolder.instance.Karts.Length; i++)
			{
				GameObject _Kart = Instantiate(KartSlotHolder.instance.Karts[i].prefab, prefabParent.transform);
				_Kart.SetActive(false);
			}
			prefabParent.transform.GetChild(index).gameObject.SetActive(true);
			#endregion
			#region button
			nextBTN.onClick.AddListener(Next);
			prevBTN.onClick.AddListener(Previous);
			selectBTN.onClick.AddListener(Select);
			#endregion
			RefreshUI();
		}

		private void Select()
		{
			GameManager.Instance.setupCharacter(KartSlotHolder.instance.Karts[index]);
		}

		void Next()
		{
			prefabParent.transform.GetChild(index).gameObject.SetActive(false);
			index = (index + 1) % KartSlotHolder.instance.Karts.Length;
			prefabParent.transform.GetChild(index).gameObject.SetActive(true);
			RefreshUI();
        }
        void Previous()
        {
            prefabParent.transform.GetChild(index).gameObject.SetActive(false);
            index--;
            if (index < 0)
            {
                index += KartSlotHolder.instance.Karts.Length;
            }
            prefabParent.transform.GetChild(index).gameObject.SetActive(true);
			RefreshUI();
        }
		void RefreshUI()
        {
			try
			{
				prefabParent.transform.GetChild(index).gameObject.SetActive(true);
				if (KartSlotHolder.instance.Karts[index].isunlocked)
				{
					selectBTN.GetComponentInChildren<Text>().text = "SELECT KART";
					lockPanel.SetActive(false);
				}
				else
				{
					selectBTN.GetComponentInChildren<Text>().text = "LOCKED";
					lockPanel.SetActive(true);
				}
				statSlider[0].fillAmount = KartSlotHolder.instance.Karts[index].Acceleration/100;
				statSlider[1].fillAmount = KartSlotHolder.instance.Karts[index].topSpeed/100;
				statSlider[2].fillAmount = KartSlotHolder.instance.Karts[index].handling/100;
				statSlider[3].fillAmount = KartSlotHolder.instance.Karts[index].toughness/100;
			}
            catch 
            {
				prefabParent.transform.GetChild(index).gameObject.SetActive(false);
				selectBTN.GetComponentInChildren<TMP_Text>().text = "LOCKED";
				lockPanel.SetActive(false);
				statSlider[0].fillAmount = 0;
				statSlider[1].fillAmount = 0;
				statSlider[2].fillAmount = 0;
				statSlider[3].fillAmount = 0;
			}
        }
	}

}

using UnityEngine;
[CreateAssetMenu(fileName = "Track", menuName = "Track")]
public class TrackClass : ScriptableObject
{
    public string nama;
    public Sprite trackImage;
    public bool isUnlocked;
    public enum TYPETRACK { KartTrack, HillTrack }
    public TYPETRACK TypeTrack;
}

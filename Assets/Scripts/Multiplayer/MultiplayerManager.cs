﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

namespace KartToon
{
    public class MultiplayerManager : MonoBehaviourPunCallbacks
    {
        public static MultiplayerManager instance;

        #region Singleton
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }

            PhotonNetwork.ConnectUsingSettings();
        }
        #endregion

        public override void OnConnectedToMaster()
        {
            Debug.Log("Connect To Master!");
        }

        /*public void MultiPlayer()
        {
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            PhotonNetwork.JoinLobby();
        }
        public override void OnJoinedLobby()
        {
            SceneManager.LoadScene("MultiplayerScene");
        }*/
    }
}
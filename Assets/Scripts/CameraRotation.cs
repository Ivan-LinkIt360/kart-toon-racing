﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour
{
    //The focal point of our camera.
    public Transform FocalPoint = null;
    //The distance from the object we want to position our camera.
    public float FocalDistance = 10.0f;
    public float HeightDistance;
    //The angle left and right relative to the object.
    public float AngleX = 0.0f;
    //The angle up and down relative to the object.
    public float AngleY = 0.0f;
    //The clamping values for the lower and upper angles of our y angle.
    //public Vector2 AngleYClamp = new Vector2(-60.0f, 60.0f);
    public Vector2 AngleXClamp = new Vector2(-60.0f, 60.0f);

    public float rotspeedY = 100.0f;
    public float rotspeedX = 100.0f;

    void Update()
    {
        #if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount==1)
        {
            AngleX += Input.GetAxis("Mouse X") * rotspeedX * Time.deltaTime;

            AngleX = Mathf.Clamp(AngleX, AngleXClamp.x, AngleXClamp.y);

            if (AngleX > 360.0f)
                AngleX -= 360.0f;

        }

        CalculateCameraPosition();
        #endif
    }

    // Calculate a new camera position based on our inputs.
    void CalculateCameraPosition()
    {
        if (FocalPoint != null)
        {
            //Calculate a camera position that's a certain distance from zero.
            Vector3 tCameraPosition = -Vector3.forward * FocalDistance;
            Vector3 hCameraPosition = Vector3.up * HeightDistance;

            //Rotate that position along the x/z axis by the AngleY the user has specified. This is a rotation relative to the origin.
            //tCameraPosition = Quaternion.Euler(AngleY, 0.0f, 0.0f) * tCameraPosition;

            //Rotate the position along the y axis by the AngleX the user has specified. This is a rotation relative to the origin.
            tCameraPosition = Quaternion.Euler(0.0f, AngleX, 0.0f) * tCameraPosition;
        
            //With our new rotated position calculated, offset it by the focal point's position. That way, if something moves the focal point, we'll keep up with it.
            transform.position = FocalPoint.transform.position + tCameraPosition;

            //Tell the transform of the camera to look at the object. This is a handy function that does some work for us.
            transform.LookAt(FocalPoint);
        }
    }
}
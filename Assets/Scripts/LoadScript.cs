﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class LoadScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadSceneFunction (string level)
	{
        SceneManager.LoadScene(level);
        PlayerPrefs.DeleteKey("JuaraBerapa");
        Time.timeScale = 1f;
	}

    public void LoadSceneFunctionMultiplayer (string level)
	{
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene(level);
        PlayerPrefs.DeleteKey("JuaraBerapa");
        Time.timeScale = 1f;
	}
}
